package be.kuleuven.assemassit.Domain.Enums;

public enum WorkPostType {
  CAR_BODY_POST, DRIVETRAIN_POST, ACCESSORIES_POST
}
