package be.kuleuven.assemassit.Domain.Enums;

public enum Color {
  RED, BLUE, BLACK, WHITE
}
