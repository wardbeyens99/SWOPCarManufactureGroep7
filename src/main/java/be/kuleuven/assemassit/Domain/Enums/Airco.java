package be.kuleuven.assemassit.Domain.Enums;

public enum Airco {
  MANUAL, AUTOMATIC
}
