package be.kuleuven.assemassit.Domain.Enums;

public enum Seat {
  LEATHER_BLACK, LEATHER_WHITE, VINYL_GREY
}
