package be.kuleuven.assemassit.Domain.Enums;

public enum Engine {
  STANDARD, PERFORMANCE
}
