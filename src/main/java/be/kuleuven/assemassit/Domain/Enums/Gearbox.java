package be.kuleuven.assemassit.Domain.Enums;

public enum Gearbox {
  MANUAL, AUTOMATIC
}
