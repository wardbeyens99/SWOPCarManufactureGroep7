package be.kuleuven.assemassit.Domain.Enums;

public enum Wheel {
  COMFORT, SPORT
}
